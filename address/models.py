#encoding=utf-8

from django.db import models
from django.core.exceptions import ObjectDoesNotExist


class Abbrev(models.Model):
    """
    Abbreviation for zones.
    """
    level = models.SmallIntegerField()
    short = models.CharField(max_length=10)
    full = models.CharField(max_length=50)
    is_prefix = models.BooleanField(default=True)

    class Meta:
        unique_together = ('level', 'short')

    def __unicode__(self):
        return u"{} ({}, level={})".format(self.full, self.short, self.level)


class ZoneLevels(object):

    REGION = 1
    AUTO_DISTRICT = 2
    DISTRICT = 3
    CITY = 4
    CITY_AREA = 5
    TOWN = 6
    STREET = 7


class ZoneManager(models.Manager):

    def actuals(self):
        qs = super(ZoneManager, self).get_queryset()
        return qs.filter(deleted=False).select_related("abbrev")

    def addr_objs(self, **params):
        return self.actuals().filter(**params)

    def regions(self):
        return self.actuals().filter(level=ZoneLevels.REGION)

    def autonomous_districts(self):
        return self.actuals().filter(level=ZoneLevels.AUTO_DISTRICT)

    def districts(self):
        return self.actuals().filter(level=ZoneLevels.DISTRICT)

    def cities(self):
        return self.actuals().filter(level=ZoneLevels.CITY)

    def towns(self):
        return self.actuals().filter(level=ZoneLevels.TOWN)

    def region_cities_and_towns(self, region_guid):
        region = self.region(guid=region_guid)
        if not region:
            return None

        levels = (ZoneLevels.CITY, ZoneLevels.TOWN, ZoneLevels.CITY_AREA,)
        return self.actuals().filter(kladr_code__startswith=region.kladr_code[:2])\
            .filter(level__in=levels).order_by("name")

    def streets(self):
        return self.actuals().filter(level=ZoneLevels.STREET)

    def addr_obj(self, **params):
        return self._get_or_none(self.actuals(), **params)

    def region(self, **params):
        return self._get_or_none(self.regions(), **params)

    def city(self, **params):
        return self._get_or_none(self.cities(), **params)

    def district(self, **params):
        return self._get_or_none(self.districts(), **params)

    def town(self, **params):
        return self._get_or_none(self.towns(), **params)

    def street(self, **params):
        return self._get_or_none(self.streets(), **params)

    def _get_or_none(self, qs, **params):
        qs = qs.filter(**params)[:1]
        if len(qs):
            return qs[0]
        return None


class Zone(models.Model):

    name = models.CharField(max_length=120, db_index=True)
    zip_code = models.CharField(max_length=6, blank=True)
    kladr_code = models.CharField(max_length=15, db_index=True)
    level = models.SmallIntegerField(db_index=True)
    short = models.CharField(max_length=10)

    abbrev = models.ForeignKey(Abbrev, on_delete=models.CASCADE)
    guid = models.CharField(max_length=36, db_index=True)
    parentguid = models.CharField(max_length=36, db_index=True)

    deleted = models.BooleanField(default=False)
    objects = ZoneManager()

    def __unicode__(self):
        return self.list_name

    @property
    def full_name(self):
        abbrev = self.abbrev
        if abbrev:
            name = u"{abbr} {name}" if abbrev.is_prefix else u"{name} {abbr}"
            name = name.format(name=self.name, abbr=self.abbrev.full)
        else:
            name = u"%s %s" % (self.short, self.name)
        return name

    @property
    def list_name(self):
        abbrev = self.abbrev
        return u"{name}, {abbr}".format(
               name=self.name,
               abbr=abbrev.full.lower() if abbrev else self.short)

    @property
    def short_name(self):
        abbrev = self.abbrev
        name = u"{abbr} {name}" if abbrev and abbrev.is_prefix\
            else u"{name} {abbr}"
        return name.format(name=self.name, abbr=self.short)

    @property
    def parent(self):
        try:
            return self.objects.get(guid=self.parentguid)
        except ObjectDoesNotExist:
            return None



class House(models.Model):
    """
    Russian buildings from KLADR
    """

    housenum = models.CharField(max_length=20)
    buildnum = models.CharField(max_length=10)
    strucnum = models.CharField(max_length=10)
    zip_code = models.CharField(max_length=6, blank=True)
    region = models.SmallIntegerField(db_index=True)
    enddate = models.DateField(db_index=True)

    guid = models.CharField(max_length=36, db_index=True)
    houseguid = models.CharField(max_length=36, db_index=True)

    def __unicode__(self):
        return ", ".join([self.housenum, self.buildnum, self.strucnum])

    @property
    def parent(self):
        try:
            return Zone.objects.get(guid=self.guid)
        except ObjectDoesNotExist:
            return None


class CurrentVersion(models.Model):

    version_id = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now_add=True)
    label = models.CharField(max_length=160)
