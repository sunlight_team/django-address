from django.apps import AppConfig


class AddressConfig(AppConfig):
    """
    Define config for the member app so that we can hook in signals.
    """
    name = 'address'
    path = '/home/vhost/sl/env/lib/python2.7/site-packages/address'