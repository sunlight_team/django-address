=================================
 django_address
=================================

:Version: 0.1.1
:Download: http://pypi.python.org/pypi/django_address/
:Source: https://bitbucket.org/gladiolus/django_address
:Keywords: address, russia, russian addresses, django, fias, kladr, ФИАС, КЛАДР, адрес, python

What is django_address for?
===========================

Django_address is django application which can be used for creating and updating the list of addresses of Russian Federation.

What do I need?
===============

Django_address runs on,

- Python (2.6, 2.7)
- Django (1.4, 1.5, 1.6)
- `dbf <https://pypi.python.org/pypi/dbf>`_ 
- `rarfile <https://pypi.python.org/pypi/rarfile/2.2>`_
- `suds <https://pypi.python.org/pypi/suds>`_

Get Started
===========
To install django_address on your system, just type::

    $ sudo pip install django_address

Now you’ve installed django_address system-wide, you’ll need to configure Django to use it. Doing so is simple; just edit your settings.py and add **'address'** to the end of **INSTALLED_APPS**.

Once django_address is added in, you’ll need to run **./manage.py syncdb** to create nessesary tables.

How to use
==========

Django_address uses dumps of database of FIAS in DBF format.  
Structure of tables explained in detail on `official site <http://fias.nalog.ru/Public/DownloadPage.aspx>`_ of FIAS.

To manage addresses custom command **update_address** should be used.

To automatically download data from FIAS' official site, parse and create address objects, type::

   $ python manage.py update_address --create  --zones

When command is over, table **address_zone** will contain actual list of russian address objects (zone, cities, streets etc).

If you need to use houses execute the following command::

   $ python manage.py update_address --create  --houses

Creating of houses can take a very long time depending on performance of your computer. 

As you created address objects and houses you can update them using following command::

   $ python manage.py update_address --update  --zones --houses


License
=======

This software is licensed under the `BSD License`. See the ``LICENSE``
file in the top distribution directory for the full license text.

.. # vim: syntax=rst expandtab tabstop=4 shiftwidth=4 shiftround



