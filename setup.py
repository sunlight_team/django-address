from setuptools import setup, find_packages

version = '0.1.1'

setup(name='django_address',
      version=version,
      description="FIAS for Django",
      long_description=open("README.rst").read(),
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Django",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='Sunlight team',
      author_email='xmdy.pro@gmail.com',
      url='',
      license='BSD',
      namespace_packages=['address'],
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
